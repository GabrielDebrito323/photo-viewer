package com.ed.photoviewer.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed.photoviewer.R
import com.ed.photoviewer.components.models.ListElement
import com.ed.photoviewer.components.recyclerview.adapter.RecyclerViewAdapter
import com.ed.photoviewer.components.recyclerview.adapter.RecyclerViewAdapter.CellType.*
import com.ed.photoviewer.components.recyclerview.TouchInteraction
import com.ed.photoviewer.app.vm.AlbumsViewModel
import com.ed.photoviewer.app.network.Resource.Status.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class PhotosFragment : Fragment(), TouchInteraction {

    private val viewModel: AlbumsViewModel by sharedViewModel()
    private val adapter: RecyclerViewAdapter = RecyclerViewAdapter(PHOTO, this)
    lateinit var rView: RecyclerView
    lateinit var pBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.restart()
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_photos, container, false)

        pBar = view.findViewById(R.id.pBarPhotos)
        rView = view.findViewById(R.id.recyclerViewPhotos)
        rView.adapter = adapter
        rView.layoutManager = LinearLayoutManager(requireContext())

        return view
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.requestPhotos.observe(viewLifecycleOwner, {
            when(it.status){
                LOADING -> {
                    pBar.visibility = VISIBLE
                }
                SUCCESS -> {
                    pBar.visibility = GONE
                    adapter.updateList(it.data as ArrayList<ListElement>)
                }
                ERROR -> {
                    pBar.visibility = GONE
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

        viewModel.requestPhotos()
    }

    companion object {
        fun newInstance() = PhotosFragment()
    }

    override fun onItemPressed(listElement: ListElement) {}
}