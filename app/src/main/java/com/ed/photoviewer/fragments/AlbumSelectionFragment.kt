package com.ed.photoviewer.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ed.photoviewer.R
import com.ed.photoviewer.components.models.Album
import com.ed.photoviewer.components.models.ListElement
import com.ed.photoviewer.components.recyclerview.adapter.RecyclerViewAdapter
import com.ed.photoviewer.components.recyclerview.adapter.RecyclerViewAdapter.CellType.*
import com.ed.photoviewer.components.recyclerview.TouchInteraction
import com.ed.photoviewer.app.vm.AlbumsViewModel
import com.ed.photoviewer.app.network.Resource.Status.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class AlbumSelectionFragment : Fragment(), TouchInteraction {


    private val albumsViewModel: AlbumsViewModel by sharedViewModel()
    private val adapter: RecyclerViewAdapter = RecyclerViewAdapter(ALBUM, this)
    private lateinit var progressbar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var input: EditText

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_album_selection, container, false)

        recyclerView = view.findViewById(R.id.rViewAlbums)
        progressbar = view.findViewById(R.id.pBarAlbums)
        input = view.findViewById(R.id.editTextSearchAlbum)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        albumsViewModel.requestAlbums.observe(viewLifecycleOwner, {
            when (it.status) {
                LOADING -> {
                    progressbar.visibility = VISIBLE
                }
                SUCCESS -> {
                    progressbar.visibility = GONE
                    adapter.updateList(list = it.data as ArrayList<ListElement>)
                }
                ERROR -> {
                    progressbar.visibility = GONE
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_LONG).show()
                }
            }
        })

        input.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(value: Editable?) {
                value?.let {
                    adapter.updateFilter(it.toString())
                }
            }
        })

        albumsViewModel.requestAlbums()
    }

    override fun onItemPressed(listElement: ListElement) {
        albumsViewModel.setSelectedAlbum(album = listElement as Album)
        activity?.let {
            it.supportFragmentManager
                .beginTransaction()
//                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
                .addToBackStack(PhotosFragment.newInstance().javaClass.name)
                .add(R.id.container, PhotosFragment.newInstance(), PhotosFragment.newInstance().javaClass.name)
                .commit()
        }
    }

    companion object {
        fun newInstance() = AlbumSelectionFragment()
    }
}