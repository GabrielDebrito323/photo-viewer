package com.ed.photoviewer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ed.photoviewer.fragments.AlbumSelectionFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().replace(
            R.id.container,
            AlbumSelectionFragment.newInstance()
        ).commit()

    }
}