package com.ed.photoviewer.components.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
class Album(
    @SerializedName("id")
    private var id: Int,
    @SerializedName("title")
    private var title: String?,
) : ListElement, Parcelable {

    override fun getMainValue(): String {
        return title?.uppercase() ?: "-"
    }

    override fun getSecondaryValue(): String {
        return id.toString()
    }

    override fun filter(query: String?): Boolean {
        query?.let {
            return title?.contains(query, true) ?: false
        } ?: run {
            return true
        }
    }
}