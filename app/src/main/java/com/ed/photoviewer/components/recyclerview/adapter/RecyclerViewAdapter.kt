package com.ed.photoviewer.components.recyclerview.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ed.photoviewer.R
import com.ed.photoviewer.components.models.ListElement
import com.ed.photoviewer.components.recyclerview.vh.AlbumViewHolder
import com.ed.photoviewer.components.recyclerview.vh.PhotoViewHolder
import com.ed.photoviewer.components.recyclerview.ReusableViewHolder
import com.ed.photoviewer.components.recyclerview.TouchInteraction
import com.ed.photoviewer.components.recyclerview.adapter.RecyclerViewAdapter.CellType.*

class RecyclerViewAdapter(val cellType: CellType, private var touchInteraction: TouchInteraction) :
    RecyclerView.Adapter<ReusableViewHolder>() {

    private var list: ArrayList<ListElement> = ArrayList()
    var filterValue: String? = null

    override fun getItemViewType(position: Int): Int {
        return when (cellType) {
            PHOTO -> R.layout.cell_photo
            ALBUM -> R.layout.cell_album
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReusableViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return when (cellType) {
            PHOTO -> PhotoViewHolder(view, touchInteraction)
            ALBUM -> AlbumViewHolder(view, touchInteraction)
        }
    }

    fun updateList(list: ArrayList<ListElement>) {
        this.list.let {
            if (it.isEmpty()){
                it.addAll(list)
                notifyDataSetChanged()
            }
        }
    }

    fun updateFilter(filterValue: String?) {
        filterValue?.let {
            this.filterValue = filterValue
            notifyDataSetChanged()
        }
    }

    private fun getFilteredList(): ArrayList<ListElement> {
        return list.filter { it.filter(filterValue) } as ArrayList<ListElement>
    }

    override fun onBindViewHolder(holder: ReusableViewHolder, position: Int) {
        var filteredList = getFilteredList()
        holder.bindView(filteredList[position])
    }

    override fun getItemCount(): Int {
        return getFilteredList().count { it.filter(filterValue) }
    }

    enum class CellType {
        PHOTO,
        ALBUM
    }
}