package com.ed.photoviewer.components.recyclerview

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.ed.photoviewer.components.models.ListElement

open class ReusableViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    open fun bindView(listElement: ListElement) {}
}