package com.ed.photoviewer.components.recyclerview.vh

import android.view.View
import android.widget.TextView
import com.ed.photoviewer.R
import com.ed.photoviewer.components.models.Album
import com.ed.photoviewer.components.models.ListElement
import com.ed.photoviewer.components.recyclerview.ReusableViewHolder
import com.ed.photoviewer.components.recyclerview.TouchInteraction

class AlbumViewHolder(view: View, touchInteraction: TouchInteraction) : ReusableViewHolder(view) {

    var listElement: ListElement? = null
    private var textViewId: TextView = view.findViewById(R.id.txtAlbumId)
    private var txtAlbumTitle: TextView = view.findViewById(R.id.txtAlbumTitle)

    init {
        itemView.setOnClickListener {
            listElement?.let { element ->
                touchInteraction.onItemPressed(element)
            }
        }
    }

    override fun bindView(listElement: ListElement) {
        this.listElement = listElement

        when(listElement){
            is Album -> {
                txtAlbumTitle.text = listElement.getMainValue()
                textViewId.text = listElement.getSecondaryValue()
            }
        }
    }

}
