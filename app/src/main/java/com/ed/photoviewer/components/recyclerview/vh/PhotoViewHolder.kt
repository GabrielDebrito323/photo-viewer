package com.ed.photoviewer.components.recyclerview.vh

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.ed.photoviewer.R
import com.ed.photoviewer.components.models.ListElement
import com.ed.photoviewer.components.models.Photo
import com.ed.photoviewer.components.recyclerview.ReusableViewHolder
import com.ed.photoviewer.components.recyclerview.TouchInteraction
import com.squareup.picasso.Picasso

class PhotoViewHolder(itemView: View, var touchInteraction: TouchInteraction) : ReusableViewHolder(itemView) {

    var listElement: ListElement? = null
    private var imgView: ImageView = itemView.findViewById(R.id.imgViewPhoto)
    private var txtPhotoTitle: TextView = itemView.findViewById(R.id.txtPhotoTitle)
    private var txtPhotoId: TextView = itemView.findViewById(R.id.txtPhotoId)

    init {
        itemView.setOnClickListener {
            listElement?.let { element ->
                touchInteraction.onItemPressed(element)
            }
        }
    }

    override fun bindView(listElement: ListElement) {
        this.listElement = listElement

        when(listElement){
            is Photo -> {
                Picasso.get()
                    .load(listElement.getPictureUrl())
                    .into(imgView)
                txtPhotoId.text = listElement.getMainValue()
                txtPhotoTitle.text = listElement.getSecondaryValue()
            }
        }
    }
}
