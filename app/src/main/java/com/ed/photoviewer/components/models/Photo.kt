package com.ed.photoviewer.components.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
class Photo(
    @SerializedName("id")
    private var id: Int,
    @SerializedName("title")
    private var title: String,
    @SerializedName("thumbnailUrl")
    private var url: String,
) : ListElement, Parcelable {

    fun getPictureUrl(): String {
        return url
    }

    override fun getMainValue(): String {
        return id.toString()
    }

    override fun getSecondaryValue(): String {
        return title.uppercase()
    }

    override fun filter(query: String?): Boolean {
        return true
    }

}