package com.ed.photoviewer.components.recyclerview

import com.ed.photoviewer.components.models.ListElement

interface TouchInteraction {

    fun onItemPressed(listElement: ListElement)
}