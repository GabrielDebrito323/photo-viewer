package com.ed.photoviewer.components.models

interface ListElement {

    fun getMainValue(): String
    fun getSecondaryValue(): String
    fun filter(query: String?): Boolean
}