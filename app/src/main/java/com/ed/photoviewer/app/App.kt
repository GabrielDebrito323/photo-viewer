package com.ed.photoviewer.app

import android.app.Application
import com.ed.photoviewer.app.di.modules.appModule
import com.ed.photoviewer.app.di.modules.retrofitModule
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(listOf(retrofitModule, appModule))
        }
    }
}