package com.ed.photoviewer.app.network

import com.ed.photoviewer.components.models.Album
import com.ed.photoviewer.components.models.Photo
import retrofit2.Callback

class Repository(var services: Services) {

    fun getAlbum(callback:Callback<ArrayList<Album>>){
        services.getAlbums().enqueue(callback)
    }

    fun getPhotos(albumId: Int, callback: Callback<ArrayList<Photo>>){
        services.getPhotos(albumId).enqueue(callback)
    }
}