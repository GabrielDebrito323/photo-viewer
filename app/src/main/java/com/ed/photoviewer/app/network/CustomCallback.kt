package com.ed.photoviewer.app.network

import com.google.gson.JsonParser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

abstract class CustomCallback<T> : Callback<T> {

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (!response.isSuccessful) {
            try {
                val errorMessage = JsonParser().parse(response.errorBody()?.string()).asJsonObject.get("message").asString
                onError(errorMessage)
            } catch (exception: Exception) {
                onError("Error desconocido")
            }
        } else if (response.body() != null) {
            onSuccess(response.body()!!)
        } else {
            onError("Error desconocido")
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        if (t is IOException) {
            onError("Error de conexion")
        } else {
            onError("error inesperado")
        }
    }

    abstract fun onSuccess(responseBody: T)

    abstract fun onError(errorMessage: String)
}