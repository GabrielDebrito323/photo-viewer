package com.ed.photoviewer.app.vm

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ed.photoviewer.app.network.CustomCallback
import com.ed.photoviewer.components.models.Album
import com.ed.photoviewer.components.models.Photo
import com.ed.photoviewer.app.network.Repository
import com.ed.photoviewer.app.network.Resource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AlbumsViewModel(private val repository: Repository) : ViewModel() {

    private var selectedAlbum: MutableLiveData<Album> = MutableLiveData()
    var requestAlbums: MutableLiveData<Resource<ArrayList<Album>>> = MutableLiveData()
    var requestPhotos: MutableLiveData<Resource<ArrayList<Photo>>> = MutableLiveData()

    fun requestAlbums() {
        requestAlbums.postValue(Resource.Loading())
        repository.getAlbum(object: CustomCallback<ArrayList<Album>>(){
            override fun onSuccess(responseBody: ArrayList<Album>) {
                requestAlbums.postValue(Resource.Success(responseBody))
            }
            override fun onError(errorMessage: String) {
                requestAlbums.postValue(Resource.Error(errorMessage))
            }
        })
    }

    fun requestPhotos() {
        requestPhotos.postValue(Resource.Loading())
        repository.getPhotos(
            albumId = selectedAlbum.value!!.getSecondaryValue().toInt(),
            callback = object : CustomCallback<ArrayList<Photo>>() {
                override fun onSuccess(responseBody: ArrayList<Photo>) {
                    requestPhotos.postValue(Resource.Success(responseBody))
                }
                override fun onError(errorMessage: String) {
                    requestPhotos.postValue(Resource.Error(errorMessage))
                }
            })
    }

    fun setSelectedAlbum(album: Album){
        selectedAlbum.value = album
    }

    fun restart() {
        requestPhotos = MutableLiveData()
    }
}