package com.ed.photoviewer.app.network

import com.ed.photoviewer.components.models.Album
import com.ed.photoviewer.components.models.Photo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface Services {

    @GET("/albums")
    fun getAlbums(): Call<ArrayList<Album>>

    @GET("/albums/{albumId}/photos")
    fun getPhotos(@Path("albumId") albumId: Int): Call<ArrayList<Photo>>

}