package com.ed.photoviewer.app.di.modules

import com.ed.photoviewer.components.Constants.BASE_URL
import com.ed.photoviewer.app.network.Services
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val retrofitModule = module {
    single { getRetrofit(get()) }
    single { getHttpClient() }
}

fun getHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .readTimeout(30L, TimeUnit.SECONDS)
        .connectTimeout(30L, TimeUnit.SECONDS)
        .callTimeout(30L, TimeUnit.SECONDS)
        .writeTimeout(30L, TimeUnit.SECONDS)
        .build()
}

fun getRetrofit(client: OkHttpClient): Services {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
        .build()
        .create(Services::class.java)
}