package com.ed.photoviewer.app.di.modules

import com.ed.photoviewer.app.vm.AlbumsViewModel
import com.ed.photoviewer.app.network.Repository
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appModule = module {

    single { Repository(get()) }
    viewModel { AlbumsViewModel(get()) }
}